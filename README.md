# JsonExcelTran

#### 介绍
Json文件和Excel文件相互转换，支持搜索查找。开发用途用于实现多语言的一些项目需求。

#### 软件使用说明  
![输入图片说明](https://images.gitee.com/uploads/images/2020/1223/170213_89f57e72_6550018.png "屏幕截图.png")    

![输入图片说明](https://images.gitee.com/uploads/images/2020/1223/170543_80f84bec_6550018.png "屏幕截图.png")    
  
打开Json文件/Excel文件，可读取出相关信息。

SearchText按钮：用于搜索，支持查找替换，搜索完成后可标记为红色。  

![输入图片说明](https://images.gitee.com/uploads/images/2020/1223/170638_ab331b0d_6550018.png "屏幕截图.png")    

![输入图片说明](https://images.gitee.com/uploads/images/2020/1223/170704_50987a04_6550018.png "屏幕截图.png")  

SaveExcel按钮：可将当前显示列保存为Excel文件   
 
SaveJsonTranEng:可保存为中英格式的Json   

![输入图片说明](https://images.gitee.com/uploads/images/2020/1223/170914_9af9b542_6550018.png "屏幕截图.png")  

SaveJsonTranCh：可保存为英中格式的Json     
![输入图片说明](https://images.gitee.com/uploads/images/2020/1223/171017_5031278b_6550018.png "屏幕截图.png")   


#### 参与贡献

1. AlbertZhao



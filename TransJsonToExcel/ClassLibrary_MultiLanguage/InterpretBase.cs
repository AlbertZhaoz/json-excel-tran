﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;                   //控件遍历所需
using System.Text.RegularExpressions;      //String处理
using Newtonsoft.Json;
using System.IO;
using System.Data;

namespace ClassLibrary_MultiLanguage
{
    public class InterpretBase
    {
        //定义字典用于储存Json配置文件资源
        public static Dictionary<string, string> resources = new Dictionary<string, string>();
        /// <summary>
        /// 配置文件加载
        /// </summary>
        /// <param name="path">配置文件绝对路径（包括文件本身）</param>
        public static void LoadFile(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return;
            }
            //var content = File.ReadAllText(path, Encoding.Default);
            var content = File.ReadAllText(path, Encoding.UTF8);
            if (!string.IsNullOrEmpty(content))
            {
                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
                foreach (string key in dict.Keys)
                {

                    if (!resources.ContainsKey(key))
                    {
                        resources.Add(key, dict[key]);
                    }
                    else
                        resources[key] = dict[key];
                }
            }
        }


        public static void SaveJson(DataTable dataTable,int columnfirst,int columnsecond)
        {          
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Json files (*.json)|*.json";
            dlg.FilterIndex = 0;
            dlg.RestoreDirectory = true;
            dlg.CreatePrompt = true;
            dlg.Title = "保存为Json文件";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                if(string.IsNullOrEmpty(dlg.FileName))
                {
                    return;
                }
                //string jsonStr = @"{""First"": ""Second""}";
                StringBuilder jsonBuilder = new StringBuilder();
                jsonBuilder.Append("{");
                jsonBuilder.Append("\n");
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    jsonBuilder.Append("\"");  
                    jsonBuilder.Append(dataTable.Rows[i][columnfirst].ToString());
                    jsonBuilder.Append("\":\"");
                    jsonBuilder.Append(dataTable.Rows[i][columnsecond].ToString());
                    jsonBuilder.Append("\",");
                    jsonBuilder.Append("\n");
                }
                jsonBuilder.Append("}");
                string jsonSave = jsonBuilder.ToString();
                //JsonConvert.DeserializeObject<DataSet>(jsonStr);
                //string output = JsonConvert.SerializeObject(dataTable,Formatting.None);             
                System.IO.File.WriteAllText(dlg.FileName, jsonSave.Remove(jsonSave.Length-3,1));
                MessageBox.Show("文件转换完成...");
            }
        }
    }
}

﻿namespace TransJsonToExcel
{
    partial class SearchText
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_FindText = new System.Windows.Forms.TextBox();
            this.textBox_ReplaceText = new System.Windows.Forms.TextBox();
            this.button_Find = new System.Windows.Forms.Button();
            this.button_Replace = new System.Windows.Forms.Button();
            this.pictureBox_FindDel = new System.Windows.Forms.PictureBox();
            this.pictureBox_ReplaceDel = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_FindDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ReplaceDel)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_FindText
            // 
            this.textBox_FindText.Location = new System.Drawing.Point(28, 12);
            this.textBox_FindText.Multiline = true;
            this.textBox_FindText.Name = "textBox_FindText";
            this.textBox_FindText.Size = new System.Drawing.Size(295, 29);
            this.textBox_FindText.TabIndex = 0;
            // 
            // textBox_ReplaceText
            // 
            this.textBox_ReplaceText.Location = new System.Drawing.Point(28, 72);
            this.textBox_ReplaceText.Multiline = true;
            this.textBox_ReplaceText.Name = "textBox_ReplaceText";
            this.textBox_ReplaceText.Size = new System.Drawing.Size(295, 29);
            this.textBox_ReplaceText.TabIndex = 1;
            // 
            // button_Find
            // 
            this.button_Find.Location = new System.Drawing.Point(387, 12);
            this.button_Find.Name = "button_Find";
            this.button_Find.Size = new System.Drawing.Size(119, 29);
            this.button_Find.TabIndex = 3;
            this.button_Find.Text = "查找";
            this.button_Find.UseVisualStyleBackColor = true;
            this.button_Find.Click += new System.EventHandler(this.button_Find_Click);
            // 
            // button_Replace
            // 
            this.button_Replace.Location = new System.Drawing.Point(389, 72);
            this.button_Replace.Name = "button_Replace";
            this.button_Replace.Size = new System.Drawing.Size(119, 29);
            this.button_Replace.TabIndex = 4;
            this.button_Replace.Text = "替换";
            this.button_Replace.UseVisualStyleBackColor = true;
            this.button_Replace.Click += new System.EventHandler(this.button_Replace_Click);
            // 
            // pictureBox_FindDel
            // 
            this.pictureBox_FindDel.Image = global::TransJsonToExcel.Properties.Resources.delete;
            this.pictureBox_FindDel.Location = new System.Drawing.Point(323, 13);
            this.pictureBox_FindDel.Name = "pictureBox_FindDel";
            this.pictureBox_FindDel.Size = new System.Drawing.Size(27, 28);
            this.pictureBox_FindDel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_FindDel.TabIndex = 2;
            this.pictureBox_FindDel.TabStop = false;
            this.pictureBox_FindDel.Click += new System.EventHandler(this.pictureBox_FindDel_Click);
            // 
            // pictureBox_ReplaceDel
            // 
            this.pictureBox_ReplaceDel.Image = global::TransJsonToExcel.Properties.Resources.delete;
            this.pictureBox_ReplaceDel.Location = new System.Drawing.Point(323, 72);
            this.pictureBox_ReplaceDel.Name = "pictureBox_ReplaceDel";
            this.pictureBox_ReplaceDel.Size = new System.Drawing.Size(27, 28);
            this.pictureBox_ReplaceDel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ReplaceDel.TabIndex = 3;
            this.pictureBox_ReplaceDel.TabStop = false;
            this.pictureBox_ReplaceDel.Click += new System.EventHandler(this.pictureBox_ReplaceDel_Click);
            // 
            // SearchText
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 125);
            this.Controls.Add(this.pictureBox_ReplaceDel);
            this.Controls.Add(this.pictureBox_FindDel);
            this.Controls.Add(this.button_Replace);
            this.Controls.Add(this.button_Find);
            this.Controls.Add(this.textBox_ReplaceText);
            this.Controls.Add(this.textBox_FindText);
            this.Name = "SearchText";
            this.Text = "SearchText";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_FindDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ReplaceDel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_FindText;
        private System.Windows.Forms.TextBox textBox_ReplaceText;
        private System.Windows.Forms.Button button_Find;
        private System.Windows.Forms.Button button_Replace;
        private System.Windows.Forms.PictureBox pictureBox_FindDel;
        private System.Windows.Forms.PictureBox pictureBox_ReplaceDel;
    }
}
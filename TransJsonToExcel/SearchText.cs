﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransJsonToExcel
{
    public partial class SearchText : Form
    {
        public DataTable dataTableFind = new DataTable();
        public Dictionary<int, int> FindColumnRow = new Dictionary<int, int>();
        public string replaceText;
        public bool find;
        public bool replace;
        public SearchText()
        {
            InitializeComponent();
        }

        private void button_Find_Click(object sender, EventArgs e)
        {         
            if (string.IsNullOrEmpty(this.textBox_FindText.Text))
            {
                MessageBox.Show("查找内容为空，请输入内容");
            }
            else
            {
               FindColumnRow.Clear();
               find = true;
                int Row = 0;
                foreach (DataRow item in dataTableFind.Rows)
                {
                    if(item[0].ToString() == this.textBox_FindText.Text)
                    {
                        FindColumnRow.Add(0, Row);
                    }
                    if(item[1].ToString() == this.textBox_FindText.Text)
                    {
                        FindColumnRow.Add(1, Row);
                    }
                    Row++;                 
                }               
                if(MessageBox.Show("共查询到:" + FindColumnRow.Count.ToString() + "处","提示",MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.Close();
                }
                else
                {
                    FindColumnRow.Clear();
                }
            }
        }

        /// <summary>
        /// 清空Find文本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_FindDel_Click(object sender, EventArgs e)
        {
            this.textBox_FindText.Text = "";
        }

        /// <summary>
        /// 清空Replace文本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_ReplaceDel_Click(object sender, EventArgs e)
        {
            this.textBox_ReplaceText.Text = "";
        }

        /// <summary>
        /// 替换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Replace_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textBox_FindText.Text))
            {
                MessageBox.Show("查找内容为空，请输入内容");
            }
            else
            {
                if(string.IsNullOrEmpty(this.textBox_ReplaceText.Text))
                {
                    MessageBox.Show("不支持输入空字符");
                }
                replace = true;
                replaceText = "";
                int Row = 0;
                foreach (DataRow item in dataTableFind.Rows)
                {
                    if (item[0].ToString() == this.textBox_FindText.Text)
                    {
                        FindColumnRow.Add(0, Row);
                    }
                    if (item[1].ToString() == this.textBox_FindText.Text)
                    {
                        FindColumnRow.Add(1, Row);
                    }
                    Row++;
                }
                if (MessageBox.Show("共替换:" + FindColumnRow.Count.ToString() + "处", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    replaceText = this.textBox_ReplaceText.Text;
                    this.Close();
                }
                else
                {
                    FindColumnRow.Clear();
                }
            }
        }
    }
}

﻿namespace TransJsonToExcel
{
    partial class JsonExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_ShowContent = new System.Windows.Forms.DataGridView();
            this.Chinese = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.English = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_OpenFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textBox_FilePath = new System.Windows.Forms.TextBox();
            this.button_Json2Excel = new System.Windows.Forms.Button();
            this.button_Excel2JsonEng = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItem_ClearDt = new System.Windows.Forms.ToolStripMenuItem();
            this.button_Excel2JsonCh = new System.Windows.Forms.Button();
            this.SearchText = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ShowContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView_ShowContent
            // 
            this.dataGridView_ShowContent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_ShowContent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ShowContent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Chinese,
            this.English});
            this.dataGridView_ShowContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_ShowContent.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_ShowContent.Name = "dataGridView_ShowContent";
            this.dataGridView_ShowContent.RowTemplate.Height = 23;
            this.dataGridView_ShowContent.Size = new System.Drawing.Size(453, 701);
            this.dataGridView_ShowContent.TabIndex = 1;
            this.dataGridView_ShowContent.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView_ShowContent_MouseDown);
            // 
            // Chinese
            // 
            this.Chinese.DataPropertyName = "First";
            this.Chinese.HeaderText = "First";
            this.Chinese.Name = "Chinese";
            // 
            // English
            // 
            this.English.DataPropertyName = "Second";
            this.English.HeaderText = "Second";
            this.English.Name = "English";
            // 
            // button_OpenFile
            // 
            this.button_OpenFile.Location = new System.Drawing.Point(12, 6);
            this.button_OpenFile.Name = "button_OpenFile";
            this.button_OpenFile.Size = new System.Drawing.Size(75, 23);
            this.button_OpenFile.TabIndex = 4;
            this.button_OpenFile.Text = "打开文件";
            this.button_OpenFile.UseVisualStyleBackColor = true;
            this.button_OpenFile.Click += new System.EventHandler(this.button_OpenFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Json文件|*.json|Excel文件|*.xlsx|Csv文件|*.csv|Excel文件|*.xls";
            // 
            // textBox_FilePath
            // 
            this.textBox_FilePath.Location = new System.Drawing.Point(97, 7);
            this.textBox_FilePath.Name = "textBox_FilePath";
            this.textBox_FilePath.Size = new System.Drawing.Size(304, 21);
            this.textBox_FilePath.TabIndex = 5;
            // 
            // button_Json2Excel
            // 
            this.button_Json2Excel.Location = new System.Drawing.Point(17, 52);
            this.button_Json2Excel.Name = "button_Json2Excel";
            this.button_Json2Excel.Size = new System.Drawing.Size(89, 23);
            this.button_Json2Excel.TabIndex = 0;
            this.button_Json2Excel.Text = "SaveExcel";
            this.button_Json2Excel.UseVisualStyleBackColor = true;
            this.button_Json2Excel.Click += new System.EventHandler(this.button_Json2Excel_Click);
            // 
            // button_Excel2JsonEng
            // 
            this.button_Excel2JsonEng.Location = new System.Drawing.Point(17, 91);
            this.button_Excel2JsonEng.Name = "button_Excel2JsonEng";
            this.button_Excel2JsonEng.Size = new System.Drawing.Size(105, 23);
            this.button_Excel2JsonEng.TabIndex = 0;
            this.button_Excel2JsonEng.Text = "SaveJsonTranEng";
            this.button_Excel2JsonEng.UseVisualStyleBackColor = true;
            this.button_Excel2JsonEng.Click += new System.EventHandler(this.button_Excel2Json_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.SearchText);
            this.splitContainer1.Panel2.Controls.Add(this.button_Excel2JsonCh);
            this.splitContainer1.Panel2.Controls.Add(this.button_Excel2JsonEng);
            this.splitContainer1.Panel2.Controls.Add(this.button_Json2Excel);
            this.splitContainer1.Size = new System.Drawing.Size(591, 736);
            this.splitContainer1.SplitterDistance = 453;
            this.splitContainer1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView_ShowContent);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(453, 701);
            this.panel2.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.button_OpenFile);
            this.panel1.Controls.Add(this.textBox_FilePath);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(453, 35);
            this.panel1.TabIndex = 6;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_ClearDt});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 26);
            // 
            // ToolStripMenuItem_ClearDt
            // 
            this.ToolStripMenuItem_ClearDt.Name = "ToolStripMenuItem_ClearDt";
            this.ToolStripMenuItem_ClearDt.Size = new System.Drawing.Size(124, 22);
            this.ToolStripMenuItem_ClearDt.Text = "清空表格";
            this.ToolStripMenuItem_ClearDt.Click += new System.EventHandler(this.ToolStripMenuItem_ClearDt_Click);
            // 
            // button_Excel2JsonCh
            // 
            this.button_Excel2JsonCh.Location = new System.Drawing.Point(17, 133);
            this.button_Excel2JsonCh.Name = "button_Excel2JsonCh";
            this.button_Excel2JsonCh.Size = new System.Drawing.Size(105, 23);
            this.button_Excel2JsonCh.TabIndex = 0;
            this.button_Excel2JsonCh.Text = "SaveJsonTranCh";
            this.button_Excel2JsonCh.UseVisualStyleBackColor = true;
            this.button_Excel2JsonCh.Click += new System.EventHandler(this.button_Excel2JsonCh_Click);
            // 
            // SearchText
            // 
            this.SearchText.Location = new System.Drawing.Point(17, 12);
            this.SearchText.Name = "SearchText";
            this.SearchText.Size = new System.Drawing.Size(89, 23);
            this.SearchText.TabIndex = 1;
            this.SearchText.Text = "SearchText";
            this.SearchText.UseVisualStyleBackColor = true;
            this.SearchText.Click += new System.EventHandler(this.SearchText_Click);
            // 
            // JsonExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 736);
            this.Controls.Add(this.splitContainer1);
            this.Name = "JsonExcel";
            this.Text = "JsonExcel";
            this.Load += new System.EventHandler(this.JsonExcel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ShowContent)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView_ShowContent;
        private System.Windows.Forms.Button button_OpenFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox textBox_FilePath;
        private System.Windows.Forms.Button button_Json2Excel;
        private System.Windows.Forms.Button button_Excel2JsonEng;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chinese;
        private System.Windows.Forms.DataGridViewTextBoxColumn English;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_ClearDt;
        private System.Windows.Forms.Button button_Excel2JsonCh;
        private System.Windows.Forms.Button SearchText;
    }
}
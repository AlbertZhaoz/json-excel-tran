﻿using ClassLibrary_MultiLanguage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransJsonToExcel
{
    public partial class JsonExcel : Form
    {
        #region 字段、属性
        private bool openFile;

        public bool m_openFile
        {
            get { return openFile; }
            set { openFile = value; }
        }
        public DataTable dataTable = new DataTable();
        #endregion
        public JsonExcel()
        {
            InitializeComponent();
        }

        private void button_OpenFile_Click(object sender, EventArgs e)
        {
            openFile = true;
            this.textBox_FilePath.Text = "";
            this.openFileDialog1.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog1.FileName) && (openFileDialog1.FileName != ""))
            {
                dataTable.Rows.Clear();
                InterpretBase.resources.Clear();
                var extension = System.IO.Path.GetExtension(openFileDialog1.FileName);
                var filePath = openFileDialog1.FileName;//获取选取文件的路径 
                if (".json"== extension)
                {                    
                    this.textBox_FilePath.Text = filePath;
                    InterpretBase.LoadFile(filePath);
                    int count = 0;
                    foreach (var item in InterpretBase.resources)
                    {
                        dataTable.Rows.Add();
                        dataTable.Rows[count]["First"] = item.Key.ToString();
                        dataTable.Rows[count]["Second"] = item.Value.ToString();
                        count++;
                    }
                    this.dataGridView_ShowContent.DataSource = dataTable;
                    this.dataGridView_ShowContent.Refresh();
                }
                else if(".xls" == extension ||".csv"==extension)
                {
                    this.textBox_FilePath.Text = filePath;
                    SaveExcel.ReadExcel(this.dataGridView_ShowContent, filePath);
                }
                
            }
        }

        private void JsonExcel_Load(object sender, EventArgs e)
        {
            dataTable.Columns.Add("First");
            dataTable.Columns.Add("Second");
        }

        ///// <summary>
        ///// 复写方法：双缓冲器打开
        ///// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

       

        private void dataGridView_ShowContent_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Right)
            {
                this.contextMenuStrip1.Show(MousePosition);
            }
        }

        private void ToolStripMenuItem_ClearDt_Click(object sender, EventArgs e)
        {
            this.dataGridView_ShowContent.DataSource = null;
            this.dataGridView_ShowContent.Refresh();
        }

       

        /// <summary>
        /// 存为Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Json2Excel_Click(object sender, EventArgs e)
        {
            SaveExcel.DataGridViewToExcel(this.dataGridView_ShowContent);
        }

        /// <summary>
        /// 存为Json
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Excel2Json_Click(object sender, EventArgs e)
        {
            DataTable dataTableJson = new DataTable();
            dataTableJson = dataGridView_ShowContent.DataSource as DataTable;
            InterpretBase.SaveJson(dataTableJson,0,1);
        }

        private void button_Excel2JsonCh_Click(object sender, EventArgs e)
        {
            DataTable dataTableJson = new DataTable();
            dataTableJson = dataGridView_ShowContent.DataSource as DataTable;
            InterpretBase.SaveJson(dataTableJson,1,0);
        }

        /// <summary>
        /// 查找Text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchText_Click(object sender, EventArgs e)
        {
            SearchText searchText = new SearchText();
            searchText.dataTableFind.Clear();
            searchText.dataTableFind = dataGridView_ShowContent.DataSource as DataTable;
            searchText.ShowDialog();
            if(searchText.find)
            {
                RefreshDataGridView();
                foreach (var item in searchText.FindColumnRow)
                {
                    this.dataGridView_ShowContent.Rows[item.Value].Cells[item.Key].Style.BackColor = Color.Red;
                }
                searchText.find = false;
            }
            else if(searchText.replace)
            {
                RefreshDataGridView();
                foreach (var item in searchText.FindColumnRow)
                {                   
                    this.dataGridView_ShowContent.Rows[item.Value].Cells[item.Key].Value = searchText.replaceText;
                    this.dataGridView_ShowContent.Rows[item.Value].Cells[item.Key].Style.BackColor = Color.Red;
                }
                searchText.replace = false;
            }
            
        }

        #region 方法：使DataGridView单元格颜色恢复为白色
        public void RefreshDataGridView()
        {
            Debug.WriteLine("开始修改颜色" + DateTime.Now.Millisecond.ToString());
            if (dataGridView_ShowContent.Rows.Count > 0)
            {
                for (int i = 0; i < dataGridView_ShowContent.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridView_ShowContent.Columns.Count; j++)
                    {
                        this.dataGridView_ShowContent.Rows[i].Cells[j].Style.BackColor = Color.White;
                    }
                }
            }
            Debug.WriteLine(DateTime.Now.Millisecond.ToString());
        }
        #endregion
    }
}
